import glob, os
import zipfile


class FileZipCompressor:

    def __init__(self, files_path, files_archive_path, archive_limit, logger):
        self.files_path = files_path
        self.files_archive_path = files_archive_path
        self.archive_limit = archive_limit
        self.logger = logger

    @staticmethod
    def check_dir_path(dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    def files_list_generator(self):
        list_files_limit = []
        os.chdir(self.files_path)

        for file in glob.glob('*.xml'):

            list_files_limit.append(file)
            if len(list_files_limit) == self.archive_limit:
                _list_files_limit = list_files_limit[:]
                list_files_limit = []

                yield _list_files_limit

    def archive_files(self):
        self.check_dir_path(self.files_archive_path)

        for _iter, files_limit_list in enumerate(self.files_list_generator(), 1):
            z_file_path = os.path.join(self.files_archive_path, 'archive_file_{}.zip'.format(_iter))
            z_archive = zipfile.ZipFile(z_file_path, mode='w')

            for _file in files_limit_list:
                z_archive.write(_file)

            z_archive.close()
            self.logger.info('Created new zip file: {}'.format(z_file_path))

