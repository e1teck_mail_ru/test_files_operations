import glob, os
import sys
import zipfile


class FileZipDecompressor:

    def __init__(self, files_archive_path, decompressed_files_path, logger):
        self.files_archive_path = files_archive_path
        self.decompressed_files_path = decompressed_files_path
        self.logger= logger

    @staticmethod
    def check_dir_path(dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    def decompress_files(self):
        self.check_dir_path(self.decompressed_files_path)
        os.chdir(self.files_archive_path)

        for zip_file in glob.glob('*.zip'):
            try:
                z_file = zipfile.ZipFile(zip_file)
                z_file.extractall(self.decompressed_files_path)
            except Exception as _err:
                self.logger.error('ERROR: {}'.format(_err))
            else:
                self.logger.info('Zip file: {} was extracted'.format(zip_file))


