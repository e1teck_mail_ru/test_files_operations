import os
import random
import uuid


class FileArchiveGenerator:

    def __init__(self, files_path, number_of_files, file_template_dict, logger):
        self.files_path = files_path
        self.file_template_dict = file_template_dict
        self.number_of_files = number_of_files
        self.logger = logger

    @staticmethod
    def check_dir_path(dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    def file_generator(self, number_of_files):

        for _ in range(0, number_of_files):
            generate_objects = lambda x: x.format(uuid.uuid4())

            file_values_dict_begin = {
                'file_id': uuid.uuid4(),
                'file_level': random.randint(1, 100)
            }
            file_objects_string = '\n'.join(
                generate_objects(self.file_template_dict['object'])
                for _ in range(random.randint(1, 10))
            )

            file_body = '{}{}{}'.format(
                self.file_template_dict['begin'].format(**file_values_dict_begin),
                file_objects_string,
                self.file_template_dict['end']
            )

            yield file_body

    def generate_files(self):
        for i, file_body in enumerate(self.file_generator(self.number_of_files), 1):
            file_name = '{}_file.xml'.format(i)
            file_path = os.path.join(self.files_path, file_name)

            with open(file_path, 'w+') as _file:
                _file.writelines(file_body)
            self.logger.info('File {} was generated'.format(file_path))

    def run_generator(self):
        self.check_dir_path(self.files_path)
        self.generate_files()


