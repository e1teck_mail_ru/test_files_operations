
import os
import time
import logging


class CustomLogger:
    """
    Log levels:
        CRITICAL = 50
        FATAL = CRITICAL
        ERROR = 40
        WARNING = 30
        WARN = WARNING
        INFO = 20
        DEBUG = 10
        NOTSET = 0
    """

    def __init__(self, logger_name, logger_level=20, use_stdout=True, logger_dir=None):
        self.logger_name = logger_name
        self.logger_dir = logger_dir
        self.logger_level = logger_level
        self.use_stdout = use_stdout
        self.logger = self.set_logger()

    def set_logger(self):

        logger = logging.getLogger(self.logger_name)
        logger.setLevel(self.logger_level)
        formatter = logging.Formatter(
            fmt='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%m-%d-%y %H:%M:%S'
        )
        if self.logger_dir:
            os.makedirs(self.logger_dir, exist_ok=True)
            filename = '{}{}.log'.format(self.logger_name, time.time())
            log_file_path = os.path.join(self.logger_dir, filename)
            handler = logging.FileHandler(log_file_path)
            handler.setFormatter(formatter)
            logger.addHandler(handler)

        if self.use_stdout or not self.logger_dir:
            handler = logging.StreamHandler()
            handler.setFormatter(formatter)
            logger.addHandler(handler)

        return logger
