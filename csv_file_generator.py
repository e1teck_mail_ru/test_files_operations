import os
import queue
import re
import threading


class CSVFileGenerator:

    def __init__(self, src_dir, output_dir, logger):
        self.src_dir = src_dir
        self.output_dir = output_dir
        self.main_queue = queue.Queue()
        self.threads_number = 4
        self.thread_list = []
        self.logger = logger

        self.objects_csv_file = None
        self.id_csv_file = None
        self.re_parse_dict = None

    @staticmethod
    def check_dir_path(dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    def thread_func(self, name):
        while True:
            get_item = self.main_queue.get()
            self.parse_file(get_item)
            self.main_queue.task_done()

    def parse_xml(self, data_str):
        results_dict = {}
        objects_list = self.re_parse_dict['list'].findall(data_str)
        if objects_list:
            results_dict['objects'] = objects_list

        parse_dict = self.re_parse_dict['dict'].search(data_str)
        if parse_dict:
            results_dict.update(parse_dict.groupdict())

        return results_dict

    def parse_file(self, file_name):
        with open(file_name, 'r') as _file:
            data_str = _file.read().replace('\n', ' ')

        parse_dict = self.parse_xml(data_str)
        self.id_csv_file.write(""""{}";"{}"\n""".format(parse_dict['id'], parse_dict['level']))

        for _object_str in parse_dict['objects']:
            self.objects_csv_file.write(""""{}";"{}"\n""".format(parse_dict['id'],_object_str))

    def start_threads(self, threads_number):
        for thread_iter in range(threads_number):
            _thread = threading.Thread(
                target=self.thread_func,
                name='Thread-{}'.format(thread_iter),
                args=('Thread-{}'.format(thread_iter),),
                daemon=True
            )

            self.thread_list.append(_thread)
            _thread.start()

            self.logger.info('Thread: {} was strted'.format(thread_iter))

    def put_files_to_queue(self):
        try:
            for _file in os.listdir(self.src_dir):
                self.main_queue.put(os.path.join(self.src_dir, _file))
        except Exception as _err:
            self.logger.error("List files error: {}".format(_err))

    def start_generate(self):
        self.check_dir_path(self.output_dir)

        self.re_parse_dict = {
            'dict': re.compile(r"^.*?name='id' value='(?P<id>[\da-z\-]+)'.*var name='level' value='(?P<level>\d+)'.*$"),
            'list': re.compile(r"<object name='([\da-z\-]+)'\s{0,}/")
        }

        self.id_csv_file = open(os.path.join(self.output_dir, 'ids_file.csv'), 'w')
        self.objects_csv_file = open(os.path.join(self.output_dir, 'objects_csv_file.csv'), 'w')
        self.id_csv_file.write(""" "id";"level"\n""")
        self.objects_csv_file.write(""""id";"object_name"\n""")

        self.start_threads(self.threads_number)
        self.put_files_to_queue()
        self.main_queue.join()
        self.id_csv_file.close()
        self.objects_csv_file.close()

        self.logger.info('CSV files was generated')
