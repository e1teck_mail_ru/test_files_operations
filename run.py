import argparse
import os

from csv_file_generator import CSVFileGenerator
from file_compresor import FileZipCompressor
from file_decompressor import FileZipDecompressor
from file_generator import FileArchiveGenerator
from logger import CustomLogger


def set_custom_args():
    """
    Устанавливаем набор аргументов
    :return:
    """

    _args = argparse.ArgumentParser(description='Files operations')
    _args.add_argument(
        '--generate_and_archive', '-ga',
        action='store_true',
        help='Files generate and archiver'
    )
    _args.add_argument(
        '--generate_csv', '-gc',
        action='store_true',
        help='Generate csv files'
    )

    return _args.parse_args()


if __name__ == '__main__':
    custom_logger = CustomLogger(logger_name='file_opperations').logger
    run_args = set_custom_args()
    root_dir = os.path.abspath(os.path.dirname(__file__))
    files_path = os.path.join(root_dir, 'files_dir')
    files_archive_path = os.path.join(root_dir, 'archive_dir')
    decompressed_files_path = os.path.join(root_dir, 'decompressed_dir')
    csv_files_dir = os.path.join(root_dir, 'csv_dir')

    file_template_dict = {
        'begin': "<root>\n"
        "    <var name='id' value='{file_id}'/>\n"
        "    <var name='level' value='{file_level}'/>\n"
        "    <objects>\n",
        'end': "\n    </objects>\n"
        "</root>",
        'object': "        <object name='{}'/>"
    }

    if run_args.generate_and_archive:
        file_generator = FileArchiveGenerator(files_path, 500, file_template_dict, logger=custom_logger)
        file_generator.run_generator()

        files_compressor = FileZipCompressor(
            files_path, files_archive_path=files_archive_path,
            archive_limit=100, logger=custom_logger
        )
        files_compressor.archive_files()

    elif run_args.generate_csv:
        file_decocmpressor = FileZipDecompressor(
            files_archive_path, decompressed_files_path,
            logger=custom_logger
        )
        file_decocmpressor.decompress_files()

        csv_generator = CSVFileGenerator(
            decompressed_files_path, csv_files_dir,
            logger=custom_logger
        )
        csv_generator.start_generate()


